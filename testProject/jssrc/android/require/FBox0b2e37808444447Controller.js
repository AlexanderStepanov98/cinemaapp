define(function(){
	var controller = require("userFBox0b2e37808444447Controller");
	var controllerActions = ["FBox0b2e37808444447ControllerActions"];

	for(var i = 0; i < controllerActions.length; i++){
		var actions = require(controllerActions[i]);
		for(var key in actions){
			controller[key] = actions[key];
		}
	}

	return controller;
})