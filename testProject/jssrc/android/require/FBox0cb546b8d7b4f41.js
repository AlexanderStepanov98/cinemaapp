define("FBox0cb546b8d7b4f41", function() {
    return function(controller) {
        FBox0cb546b8d7b4f41 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "23%",
            "id": "FBox0cb546b8d7b4f41",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "width": "100%"
        }, {
            "containerWeight": 100
        }, {});
        FBox0cb546b8d7b4f41.setDefaultUnit(kony.flex.DP);
        var imgFilm = new kony.ui.Image2({
            "centerY": "50%",
            "height": "123dp",
            "id": "imgFilm",
            "imageWhileDownloading": "film2.png",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "top": "0dp",
            "width": "135dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var txtFilm = new kony.ui.RichText({
            "centerY": "50%",
            "height": "123dp",
            "id": "txtFilm",
            "isVisible": true,
            "left": "141dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0b891ade697f644",
            "text": "RichText",
            "top": "0dp",
            "width": "38.86%",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "hExpand": true,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var btnScreening = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "defBtnFocus",
            "height": "50dp",
            "id": "btnScreening",
            "isVisible": true,
            "left": "312dp",
            "skin": "CopydefBtnNormal0bf16b754df4f40",
            "text": "Button",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "hExpand": true,
            "margin": [6, 6, 6, 6],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        var txtGenre = new kony.ui.RichText({
            "centerX": "54.63%",
            "height": "15dp",
            "id": "txtGenre",
            "isVisible": true,
            "left": "171dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0f249fac3bfb34a",
            "text": "RichText",
            "top": "100dp",
            "width": "34.26%",
            "zIndex": 1
        }, {
            "containerWeight": 100,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "hExpand": true,
            "margin": [0, 0, 0, 0],
            "marginInPixel": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "vExpand": false,
            "widgetAlignment": constants.WIDGET_ALIGN_CENTER
        }, {});
        FBox0cb546b8d7b4f41.add(imgFilm, txtFilm, btnScreening, txtGenre);
        return FBox0cb546b8d7b4f41;
    }
})