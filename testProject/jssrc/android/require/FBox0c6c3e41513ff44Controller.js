define(function(){
	var controller = require("userFBox0c6c3e41513ff44Controller");
	var controllerActions = ["FBox0c6c3e41513ff44ControllerActions"];

	for(var i = 0; i < controllerActions.length; i++){
		var actions = require(controllerActions[i]);
		for(var key in actions){
			controller[key] = actions[key];
		}
	}

	return controller;
})