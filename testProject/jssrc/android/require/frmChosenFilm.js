define("frmChosenFilm", function() {
    return function(controller) {
        function addWidgetsfrmChosenFilm() {
            this.setDefaultUnit(kony.flex.DP);
            var contChosenFilm = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "715dp",
                "horizontalScrollIndicator": true,
                "id": "contChosenFilm",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0a2e7173dcd6048",
                "top": "4dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            contChosenFilm.setDefaultUnit(kony.flex.DP);
            var txtChosenFilmTitle = new kony.ui.RichText({
                "height": "42dp",
                "id": "txtChosenFilmTitle",
                "isVisible": true,
                "left": "15dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0fc3b35fde3d646",
                "text": "Хеллоуин",
                "top": "246dp",
                "width": "49.97%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "centerX": "50.00%",
                "focusSkin": "defTextAreaFocus",
                "height": "285dp",
                "id": "txtDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "15dp",
                "numberOfVisibleLines": 3,
                "placeholder": "Placeholder",
                "skin": "CopydefTextAreaNormal0ef739321ebac45",
                "text": "Год: 2018\nСтрана: США\nРежиссеры: Дэвид Гордон Грин\t\nСтудия: Universal Pictures, Miramax Films, Blumhouse Productions\nЖанр: триллер, ужасы\nБюджет: 10 млн. долл. США\nПремьера (в Украине): 25.10.2018\nПремьера (в мире): 08.09.2018\nПродолжительность: 106 мин.\nВозрастные ограничения (лет): 18\nДистрибьютор: B&H\nРейтинг  IMDB: 7.3/10 ★",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "304dp",
                "width": "385dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "placeholderSkin": "defTextAreaPlaceholder"
            });
            var txtStory = new kony.ui.RichText({
                "height": "77dp",
                "id": "txtStory",
                "isVisible": true,
                "left": "15dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0d619ac2ffa404c",
                "text": "Хэллоуинская ночь в жизни Лори Строуд давно перестала быть каким-то праздником. После событий 1978 года",
                "top": "585dp",
                "width": "92.75%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtReadMore = new kony.ui.RichText({
                "height": "28dp",
                "id": "txtReadMore",
                "isVisible": true,
                "left": "88dp",
                "linkSkin": "defRichTextLink",
                "skin": "CopydefRichTextNormal0c9fe6f368ca440",
                "text": "read more",
                "top": "627dp",
                "width": "20.92%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnScreening = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "44dp",
                "id": "btnScreening",
                "isVisible": true,
                "left": "300dp",
                "skin": "CopydefBtnNormal0f1b02b96f82c49",
                "text": "Screenings",
                "top": "246dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            kony.mvc.registry.add('FBox0b2e37808444447', 'FBox0b2e37808444447', 'FBox0b2e37808444447Controller');
            var segmChosenFilm = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "data": [{
                    "imgChosenFilm": "film2wide.png"
                }, {
                    "imgChosenFilm": "film2wide2.png"
                }, {
                    "imgChosenFilm": "film2wide3.png"
                }, {
                    "imgChosenFilm": "film2wide4.png"
                }],
                "groupCells": false,
                "height": "238dp",
                "id": "segmChosenFilm",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "Copyseg0i992a1ab32d347",
                "rowTemplate": "FBox0b2e37808444447",
                "scrollingEvents": {},
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "-4dp",
                "viewType": constants.SEGUI_VIEW_TYPE_PAGEVIEW,
                "widgetDataMap": {
                    "imgChosenFilm": "imgChosenFilm"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            contChosenFilm.add(txtChosenFilmTitle, txtDescription, txtStory, txtReadMore, btnScreening, segmChosenFilm);
            this.add(contChosenFilm);
        };
        return [{
            "addWidgets": addWidgetsfrmChosenFilm,
            "enabledForIdleTimeout": false,
            "id": "frmChosenFilm",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0he3b5893fab34f"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "footerOverlap": false,
            "headerOverlap": false,
            "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
            "retainScrollPosition": false,
            "titleBar": true,
            "titleBarSkin": "slTitleBar",
            "windowSoftInputMode": constants.FORM_ADJUST_PAN
        }]
    }
});