define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for ticketsBtn **/
    AS_Button_a3d30f008ca849039f799da3760d2e42: function AS_Button_a3d30f008ca849039f799da3760d2e42(eventobject) {
        var self = this;
        return self.goToScreenings.call(this);
    },
    /** onTouchStart defined for forwardLabel **/
    AS_Label_cd28a1ec3d4042a994487f4357d32d46: function AS_Label_cd28a1ec3d4042a994487f4357d32d46(eventobject, x, y) {
        var self = this;
        return self.goToScreenings.call(this);
    },
    /** onTouchStart defined for backLabel **/
    AS_Label_h186d5eab84544149e5c4ff45a1a5952: function AS_Label_h186d5eab84544149e5c4ff45a1a5952(eventobject, x, y) {
        var self = this;
        return self.goBack.call(this);
    }
});