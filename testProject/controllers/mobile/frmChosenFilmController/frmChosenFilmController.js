define({ 

  data : {},
  
  onNavigate: function (context) {
    //  uncomment this later
    //  this.data = context;
    var serviceName = "getCinemas";
    var getFilm = "getFilmById";
    var getActors = "getActorsByFilmId";
    var getImagesId = "getFilmImagesId";
    var integrationClient = null;
    var params = {
      //  uncomment this later
      //  filmId: context.filmId,
      filmId: 7, // test value
      apiKey: "pol1kh111"
    };
    var imgParams = {
      imgId: null,
      apiKey: "pol1kh111"
    };

    try {
      integrationClient = kony.sdk.getCurrentInstance().getIntegrationService(serviceName);
      integrationClient.invokeOperation(getFilm, null, params, correctFilmResponse, errorFilmResponse);
      integrationClient.invokeOperation(getActors, null, params, correctActorsResponse, errorActorsResponse);
      integrationClient.invokeOperation(getImagesId, null, params, correctImgIdResponse, errorImgIdResponse);
    } catch (e) {
      alert("Error: " + e.message);
    }
	
    function correctFilmResponse (response) {
      var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
      var i;
      frmChosenFilm.view.genreDescription.text = "";
      frmChosenFilm.view.filmTitleLabel.text = response.title;   
      frmChosenFilm.view.durationDescription.text = response.duration;
      frmChosenFilm.view.ratingDescription.text = response.rating;
      frmChosenFilm.view.yearDescription.text = response.year;
      frmChosenFilm.view.plotDescription.text = response.description;
      for (i = 0; i < response.genre.length; i++) {
        frmChosenFilm.view.genreDescription.text += response.genre[i];
      }
    }
    
    function correctActorsResponse (response) {
      var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
      frmChosenFilm.view.actorsDescription.text = "";
      var i;
      for (i = 0; i < response.persons.length; i++) {
        frmChosenFilm.view.actorsDescription.text += response.persons[i].first_name + " " + response.persons[i].last_name + "\n";
      }
    }
    
    function correctImgIdResponse (response) {
      var i;
      var frmChosenFilm = _kony.mvc.GetController("frmChosenFilm", true);
      frmChosenFilm.view.imgSegmentSlider.removeAll();
      for(i = 0; i < response.numberOfElements; i++) {
        imgParams.imgId = response.content[i].id;
        frmChosenFilm.view.imgSegmentSlider.addDataAt({
          filmImg: {src: "http://kino-teatr.ua:8081/services/api/film/image/" + imgParams.imgId + "?apiKey=" + imgParams.apiKey + "&width=300&height=400&ratio=1"}
        }, i);
      }
    }
    
    function errorFilmResponse (error) {
      alert("FilmResponse Error");
    }
    
    function errorActorsResponse (error) {
      alert("ActorsResponse Error");
    }   
    
    function errorImgIdResponse (error) {
      alert("ImgIdResponse Error");
    }    
  },
  
  goToScreenings: function () {
    var ntf = new kony.mvc.Navigation("frmScreenings");
	ntf.navigate(this.data);
  },
  
  goBack: function () {
    var ntf = new kony.mvc.Navigation("frmCatalog");
    ntf.navigate();
  }
  
});